# Simple Express Server

A simple server with JWT authentication.

## Get Started:

### Download the repository and install all dependencies :

```
git clone https://gitlab.com/hanishraj/express-server.git
cd express-server
npm install
```

### Configure server environment variables:

```
cp .env.example .env
```

### Generate Prisma Client

```
npx prisma generate
```

### Run Prisma migration

```
npx prisma migrate
```

### Run the server:

```
npm run start
```

## Endpoints:

- /signin - POST (email, password)
- /signup - POST (email, password)
- /articles POST (protected) (email, password)
- /article POST (protected) (name, content)
- /article/:id PUT (protected) (name, content)
- /article/:id DELETE (protected)
- /user - GET (protected)

const Authentication = require("./controllers/authentication");
const Article = require("./controllers/article");
const passport = require("passport");

require("./services/passport");

const requireAuth = passport.authenticate("jwt", { session: false });
const requireSignIn = passport.authenticate("local", { session: false });

module.exports = function (app) {
  app.get("/", function (req, res) {
    res.send("An Express Server");
  });

  //User

  app.post("/signin", requireSignIn, Authentication.signin);
  app.post("/signup", Authentication.signup);
  app.get("/user", requireAuth, function (req, res) {
    res.send({ userDetail: { "username": req.user.email.split("@")[0], "email": req.user.email } });
  });

  //Article

  app.post('/article', requireAuth, Article.postArticle);
  app.put('/article/:id', requireAuth, Article.putArticle);
  app.delete('/article/:id', requireAuth, Article.deleteArticle);
  app.get('/articles', requireAuth, Article.getArticles);

};

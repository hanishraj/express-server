const db = require("../db/index");

exports.postArticle = async (req, res, next) => {

    const article = await db.article.create({
        data: {
            name: req.body.name,
            content: req.body.content,
            authorid: req.user.id
        },
    });

    return res.json({ res: "Created Successfully " + true, data: article });

}

exports.putArticle = async (req, res, next) => {
    try {
        const { id } = req.params
        await db.article.update({
            where: { id: String(id) },
            data: {
                name: req.body.name,
                content: req.body.content,
                authorid: req.user.id
            }
        })
        const articles = await db.article.findMany({
            where: {
                id: String(id),
            },
            orderBy: {
                name: 'desc'
            },
            include: {
                author: true
            }
        });
        return res.json({
            res: "Updated Successfully " + true,
            articles
        })
    } catch (error) {
        return res.json({
            res: false,
            error
        })
    }

}

exports.deleteArticle = async (req, res, next) => {

    const { id } = req.params
    try {
        await db.article.delete({
            where: {
                id: String(id),
            },
        })

        return res.json({
            res: "Deleted Successfully " + true,
            article
        })
    } catch (error) {
        return res.json({
            res: false,
            error
        })
    }
}

exports.getArticles = async (req, res, next) => {

    try {
        const articles = await db.article.findMany({
            where: {
                authorid: req.user.id
            },
            orderBy: {
                name: 'desc'
            },
            include: {
                author: true
            }
        });
        return res.json({
            res: "Fetched Successfully " + true,
            articles
        })
    } catch (error) {
        return res.json({
            res: false,
        })
    }
};


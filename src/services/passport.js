const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local");
const { secret } = require("../config");
const db = require("../db");
const { comparePasswords } = require("../utils/password");

// Local Strategy:
const localOptions = { usernameField: "email" };
const localLogin = new LocalStrategy(
  localOptions,
  async (email, password, done) => {
    const user = await db.user.findFirst({
      where: {
        email: {
          equals: email,
        },
      },
    });

    if (!user) {
      return done(null, false);
    }

    const isMatch = await comparePasswords(password, user.password);

    if (!isMatch) {
      return done(null, false);
    }

    return done(null, user);
  }
);

//JWT Strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: secret,
};

const jwtLogin = new JwtStrategy(jwtOptions, async (payload, done) => {
  const user = await db.user.findUnique({
    where: {
      id: payload.sub,
    },
  });

  if (user) {
    done(null, user);
  } else {
    done(null, false);
  }
});

//Using Defined Strategies in Passport:
passport.use(jwtLogin);
passport.use(localLogin);
